# Mirror

Mirror of container images consumed by the CRI.

Inspired by [GitLab's mirror](https://gitlab.com/gitlab-org/cloud-native/mirror/images).

This mirror is not an active mirror from Docker Hub. We only mirror the specific tagged images consumed by related projects.
