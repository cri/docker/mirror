#!/bin/sh

if [ $# -eq 0 ] ; then
  echo 'No file specified'
  exit 0
fi

set -e

image_file=$1

if [ ! -f ${image_file} ] ; then
  echo "Image file not found: ${image_file}";
  exit 1
fi

DIGEST_DIR=${DIGEST_DIR:-"digest"}
mkdir -p $DIGEST_DIR

while read -r image mirror_image; do
  mirror_path="docker://${CI_REGISTRY_IMAGE}/${mirror_image}"
  digest_path=${DIGEST_DIR}/$(basename $image)

  if [ -z ${DRY_RUN+x} ]; then
    echo "Pushing ${mirror_path} to mirror registry"
    skopeo --insecure-policy copy --all --src-no-creds \
      --digestfile $digest_path \
      docker://${image} ${mirror_path}
  else
    echo "DRY RUN: not pushing ${mirror_path} to mirror registry."
    echo "Checking that ${image} exists in public registry"
    skopeo --insecure-policy inspect docker://${image}
  fi


  echo "Synced ${digest_path}, manifest digest: $(cat ${digest_path})"
done < "${image_file}"
